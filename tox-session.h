#ifndef __TOX_SESSION_H__
#define __TOX_SESSION_H__

typedef struct ToxSession
{
  GObject parent;

  struct _ToxSessionPrivate *priv;
} ToxSession;

typedef struct ToxSessionClass
{
  GObjectClass parent;
} ToxSessionClass;

GType tox_session_get_type(void);
#define TOX_TYPE_SESSION         (tox_session_get_type())
#define TOX_SESSION(object)      (G_TYPE_CHECK_INSTANCE_CAST((object), TOX_TYPE_SESSION, ToxSession))
#define TOX_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), TOX_TYPE_SESSION, ToxSessionClass))
#define TOX_IS_SESSION(object)   (G_TYPE_CHECK_INSTANCE_TYPE((object), TOX_TYPE_SESSION))
#define TOX_IS_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), TOX_TYPE_SESSION))
#define TOX_SESSION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), TOX_TYPE_SESSION, ToxSessionClass))

#endif /* __TOX_SESSION_H__ */
