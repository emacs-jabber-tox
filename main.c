#include <farsight/farsight.h>
#include <dbus/dbus-glib.h>
#include <glib/gprintf.h>
#include <string.h>
#include "tox.h"

int
main(int argc, char *argv[])
{
  GMainLoop *loop;
  DBusGConnection *connection;
  DBusGProxy *bus_proxy;
  GError *error = NULL;
  ToxObject *obj;
  guint32 x;

  g_type_init();
  gst_init(&argc, &argv);

  {
    GLogLevelFlags fatal_mask;
    
    fatal_mask = g_log_set_always_fatal (G_LOG_FATAL_MASK);
    /* not aborting on warnings because of:
       farsight-rtp-WARNING **: Not enough information in rtp caps
    */
    fatal_mask |= /*G_LOG_LEVEL_WARNING | */ G_LOG_LEVEL_CRITICAL;
    g_log_set_always_fatal (fatal_mask);
  }

  connection = dbus_g_bus_get(DBUS_BUS_SESSION, &error);
  if (connection == NULL) {
    g_printerr("Failed to open connection to bus: %s\n",
	       error->message);
    exit(1);
  }

  bus_proxy = dbus_g_proxy_new_for_name(connection, "org.freedesktop.DBus",
					"/org/freedesktop/DBus",
					"org.freedesktop.DBus");

  g_debug("About to request D-Bus name...\n");
  if (!dbus_g_proxy_call(bus_proxy, "RequestName", &error,
			 G_TYPE_STRING, "net.sourceforge.emacs-jabber.Tox",
			 G_TYPE_UINT, 
			 /* these settings are good for debugging.  we
			    should listen for the NameLost signal, though. */
			 DBUS_NAME_FLAG_ALLOW_REPLACEMENT
			 | DBUS_NAME_FLAG_REPLACE_EXISTING
			 | DBUS_NAME_FLAG_DO_NOT_QUEUE,
			 G_TYPE_INVALID,
			 G_TYPE_UINT, &x,
			 G_TYPE_INVALID)) {
    g_printerr("Couldn't acquire name: %s\n", error->message);
    exit(1);
  }
  else if (x != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
    g_printerr("Couldn't acquire name: RequestName returned %u\n", x);
    exit(1);
  }
  g_debug("D-Bus name acquired\n");

  obj = g_object_new(TOX_TYPE_OBJECT, "dbus-connection", connection, NULL);
  dbus_g_connection_register_g_object(connection, "/net/sourceforge/emacs_jabber/Tox", G_OBJECT(obj));

  loop = g_main_loop_new(NULL, FALSE);

  g_main_loop_run(loop);
  return 0;
}
