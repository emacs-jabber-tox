#ifndef __TOX_H__
#define __TOX_H__

typedef struct ToxObject
{
  GObject parent;

  struct _ToxObjectPrivate *priv;
} ToxObject;

typedef struct ToxObjectClass
{
  GObjectClass parent;
} ToxObjectClass;

GType tox_object_get_type(void);
#define TOX_TYPE_OBJECT         (tox_object_get_type())
#define TOX_OBJECT(object)      (G_TYPE_CHECK_INSTANCE_CAST((object), TOX_TYPE_OBJECT, ToxObject))
#define TOX_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), TOX_TYPE_OBJECT, ToxObjectClass))
#define TOX_IS_OBJECT(object)   (G_TYPE_CHECK_INSTANCE_TYPE((object), TOX_TYPE_OBJECT))
#define TOX_IS_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), TOX_TYPE_OBJECT))
#define TOX_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), TOX_TYPE_OBJECT, ToxObjectClass))

#endif /* __TOX_H__ */
