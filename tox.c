#include <dbus/dbus-glib.h>
#include <glib/gprintf.h>
#include "tox.h"
#include "tox-session.h"

typedef struct _ToxObjectPrivate {
  guint session_counter;
  DBusGConnection *connection;
} ToxObjectPrivate;

G_DEFINE_TYPE(ToxObject, tox_object, G_TYPE_OBJECT)

gboolean tox_create_session(ToxObject *obj, guint8 dir, char **ret, GError **error);

/* properties */
enum {
  TOX_DBUS_CONNECTION = 1,
};

static void tox_set_property(GObject *obj, guint property_id, const GValue *value, GParamSpec *pspec);
static void tox_get_property(GObject *obj, guint property_id, GValue *value, GParamSpec *pspec);

#include "tox-object-glue.h"

void
tox_object_init(ToxObject *obj)
{
  obj->priv = g_new0(ToxObjectPrivate, 1);
}

void
tox_object_class_init(ToxObjectClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  GParamSpec *connection_param_spec;

  gobject_class->set_property = tox_set_property;
  gobject_class->get_property = tox_get_property;

  connection_param_spec = g_param_spec_boxed("dbus-connection",
					     "D-Bus connection",
					     "Connection where new sessions are registered",
					     DBUS_TYPE_G_CONNECTION,
					     G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  g_object_class_install_property(gobject_class,
				  TOX_DBUS_CONNECTION,
				  connection_param_spec);

  dbus_g_object_type_install_info(TOX_TYPE_OBJECT, &dbus_glib_tox_object_info);

}

static void
tox_set_property(GObject *obj, guint property_id, const GValue *value, GParamSpec *pspec)
{
  ToxObject *self = (ToxObject *)obj;

  switch(property_id) {
  case TOX_DBUS_CONNECTION:
    /* XXX: this should be a weak ref */
    /* hm? */
/*     if (self->priv->connection) */
/*       dbus_g_connection_unref(self->priv->connection); */
    self->priv->connection = (DBusGConnection *)g_value_get_boxed(value);
/*     dbus_g_connection_ref(self->priv->connection); */
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
  }
}

static void
tox_get_property(GObject *obj, guint property_id, GValue *value, GParamSpec *pspec)
{
  ToxObject *self = (ToxObject *)obj;

  switch(property_id) {
  case TOX_DBUS_CONNECTION:
    g_value_set_boxed(value, self->priv->connection);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
  }
}

gboolean
tox_create_session(ToxObject *obj, guint8 dir, char **ret, GError **error)
{
  guint session_number = ++obj->priv->session_counter;
  DBusGConnection *connection = obj->priv->connection;
  ToxSession *session;

  if (dir < 1 || dir > 3) {
    /* XXX: how to set the D-Bus error name? */
    g_set_error(error, DBUS_GERROR,
		DBUS_GERROR_REMOTE_EXCEPTION,
		"Direction out of range (was %u, should be 1 <= dir <= 3)",
		dir);
    return FALSE;
  }

  session = g_object_new(TOX_TYPE_SESSION, "direction", dir, NULL);

  *ret = g_strdup_printf("/net/sourceforge/emacs_jabber/Tox/%u", session_number);
  dbus_g_connection_register_g_object(connection, *ret, G_OBJECT(session));

  g_debug("CreateSession called, returning %s\n", *ret);

  return TRUE;
}
